angular.module('app', [])
	.directive('foo', function() {
    return function($scope, element, attrs) {
        // ---***** Инициализация карты
ymaps.ready(init);
    var myMap;
    
    function showMar(){
      ymaps.route($scope.wayMap).then(
        function (route) {
          route.getPaths().options.set({
            strokeColor: $scope.wayColor,
            opacity: 1.0,
            strokeWidth: 5
          });
                myMap.geoObjects.add(route.getPaths());
        
              $scope.$watch($scope.wayMap, function() {
                    setTimeout(function(){ 
                      myMap.geoObjects.remove(route.getPaths()); 
                    }, 500);
                  showMar();          
              });
        },
      function (error) {
          alert('Error occurred: ' + error.message);
      });
    };

    function init(){     
        myMap = new ymaps.Map('map', {
            center: [50.906417, 34.800471],
            zoom: 12
        });
      showMar();
    };
// ---***** 
  }
})
  .directive('hdrlng', function() {
    return{
       template: "<title>{{titLng}}</title>",
          link: function($scope, element, attrs) {
          }
    };
  })

	.controller('myCtrl', function($scope, $http) {

      $http.get('datalist.json').success(function(data) {
          $scope.list = data;
      });
            $http.get('trallist.json').success(function(data) {
                $scope.listT = data;
            });

$scope.wayMap = ['51.877174, 34.792831', '51.895501, 34.780366'];
$scope.busWay="";
$scope.marWay="";
$scope.listTmp=[];
$scope.listTmpT=[];

$scope.buttonPress = function(id) {
  if ($($scope.ID).attr('class') == 'but marp') {$($scope.ID).attr('class', 'but')}
    else {$($scope.ID).attr('class', 'butT')};	

  $scope.busWay = $scope.list[id-1].checkPoint;
  $scope.wayMap = $scope.list[id-1].pnts;
  $scope.ID = '#mar'+id;
  $scope.wayColor = '#FF0000'
  $($scope.ID).attr('class', 'but marp');
};

$scope.buttonPressT = function(id) {
  if ($($scope.ID).attr('class') == 'butT marpT') {$($scope.ID).attr('class', 'butT')}
    else {$($scope.ID).attr('class', 'but')};

  $scope.busWay = $scope.listT[id-1].checkPoint;
  $scope.wayMap = $scope.listT[id-1].pnts;
  $scope.ID = '#marT'+id;
  $scope.wayColor = '#1E90FF'
  $($scope.ID).attr('class', 'butT marpT');
}


$scope.mouseOvr = function(id) {
  $scope.marWay = $scope.list[id-1].checkPoint[0]+" - "+$scope.list[id-1].checkPoint[$scope.list[id-1].checkPoint.length-1];
  $scope.marColor = '#FF0000';
};

$scope.mouseOvrT = function(id) {
  $scope.marWay = $scope.listT[id-1].checkPoint[0]+" - "+$scope.listT[id-1].checkPoint[$scope.listT[id-1].checkPoint.length-1];
  $scope.marColor = '#1E90FF';
};

$scope.mouseLv = function(id) {
  $scope.marWay = '';
};

$scope.setlistTmp = function() {
  if ($scope.listTmp.length < 1) {
    for (i=0;i<$scope.list.length;i++) {
      $scope.listTmp[i] = $scope.list[i].checkPoint;
    };
    for (i=0;i<$scope.listT.length;i++) {
      $scope.listTmpT[i] = $scope.listT[i].checkPoint;
    };
  };
};

$scope.setLng = function(x) {

  if (x == 'gb') {
    $('#flagGB').attr('class', 'lng flagon');
    $('#flagRU').attr('class', 'lng');
    $('#flagUA').attr('class', 'lng');

    $scope.titLng = "Sumy's public transport system.";
    $scope.marLng = "Route";
    $scope.filterLng = "Filter";
    $scope.altLng = "Minibus"
    $scope.altLngT = "Trolleybus"

      $scope.setlistTmp();

  for (i=0;i<$scope.list.length;i++) {
    $scope.list[i].checkPoint = $scope.list[i].checkPointGB;
  };
      for (i=0;i<$scope.listT.length;i++) {
        $scope.listT[i].checkPoint = $scope.listT[i].checkPointGB;
      };
  $scope.busWay="";

};
  if (x == 'ua') {
    $('#flagGB').attr('class', 'lng');
    $('#flagRU').attr('class', 'lng');
    $('#flagUA').attr('class', 'lng flagon');

    $scope.titLng = "Транспортна інфраструктура міста Суми.";
    $scope.marLng = "Маршрут";
    $scope.filterLng = "Фільтр";
    $scope.altLng = "Маршрутка"
    $scope.altLngT = "Тролейбус"

    $scope.setlistTmp();

  for (i=0;i<$scope.list.length;i++) {
      $scope.list[i].checkPoint = $scope.list[i].checkPointUA;
  };
    for (i=0;i<$scope.listT.length;i++) {
      $scope.listT[i].checkPoint = $scope.listT[i].checkPointUA;
    };

  $scope.busWay="";

};

  if (x == 'ru') {
    $('#flagGB').attr('class', 'lng');
    $('#flagRU').attr('class', 'lng flagon');
    $('#flagUA').attr('class', 'lng');

    $scope.titLng = "Транспортная инфраструктура города Сумы.";
    $scope.marLng = "Маршрут";
    $scope.filterLng = "Фильтр";
    $scope.altLng = "Маршрутка"
    $scope.altLngT = "Троллейбус"

  if ($scope.listTmp.length > 1) {
      for (i=0;i<$scope.list.length;i++) {
        $scope.list[i].checkPoint = $scope.listTmp[i];
      };
        for (i=0;i<$scope.listT.length;i++) {
          $scope.listT[i].checkPoint = $scope.listTmpT[i];
        };
    };
  $scope.busWay="";
  };
};
    $scope.setLng('ru'); 
})